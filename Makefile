presentation.html : presentation.md script.html
	pandoc presentation.md script.html --css style.css -s -o presentation.html

clean:
	rm presentation.html

.PHONY : clean