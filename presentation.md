---
title: APconf 2019

---
<section>

### Hi! I'm **Michalis**, or as you might know me from the fediverse, **qwazix**
![me^](avatar.png)

</section><section>

### One month ago I submitted a talk titled
## I don't know what I'm talking about

</section><section>

## Now I do!

</section><section>

## Let's begin!

> It's a pretty involved project, building a new federated app from scratch. But it's limited in scope -- all API, no frontend needed. Probably / hopefully not too time consuming.

<p style=align-self:flex-end >(Matt Baer)</p>

<!-- It's been about a month since Matt posted this in the team chat. -->

</section><section>

## Reading the spec

![alyssa](alyssa-bubble-production.svg)

<!-- I decided to go read the spec. It's one of the best specs I've ever seen, but it's still a spec -->

</section><section>

## Ok let's curl 

`curl https://fosstodon.org/@qwazix --header "content-type: application/json"`

</section><section>

<div class=dump>

```
<!DOCTYPE html>
<html lang='en'>
<head>
<meta charset='utf-8'>
<meta content='width=device-width, initial-scale=1' name='viewport'>
<link href='/favicon.ico' rel='icon' type='image/x-icon'>
<link href='/apple-touch-icon.png' rel='apple-touch-icon' sizes='180x180'>
<link color='#2B90D9' href='/mask-icon.svg' rel='mask-icon'>
<link href='/manifest.json' rel='manifest'>
<meta content='/browserconfig.xml' name='msapplication-config'>
<meta content='#282c37' name='theme-color'>
<meta content='yes' name='apple-mobile-web-app-capable'>
<title>qwazix (@qwazix@fosstodon.org) - Fosstodon</title>
<link rel="stylesheet" media="all" href="/packs/css/common-2e494388.css" />
<link rel="stylesheet" media="all" href="/packs/css/default-26ae471d.chunk.css" />
<script src="/packs/js/common-35192239448008187dbe.js" crossorigin="anonymous"></script>
<script src="/packs/js/locale_en-82715cdb7f69b54d4e61.chunk.js" crossorigin="anonymous"></script>
<meta name="csrf-param" content="authenticity_token" />
<meta name="csrf-token" content="CKyjFYOsP3FEdx4vnZYVli9orSxPLF6b3/SOAtxHhLgjJY/CHgXRk3Uoa2M/f2/4+81VEtctrCqGJdGYbMss3w==" />
<meta content='10 Toots, 9 Following, 9 Followers · all the things code: fediverse/3d printing' name='description'>
<link href='https://fosstodon.org/api/salmon/36656' rel='salmon'>
<link href='https://fosstodon.org/users/qwazix.atom' rel='alternate' type='application/atom+xml'>
<link href='https://fosstodon.org/users/qwazix.rss' rel='alternate' type='application/rss+xml'>
<link href='https://fosstodon.org/users/qwazix' rel='alternate' type='application/activity+json'>
<meta content="profile" property="og:type" />
<meta content='10 Toots, 9 Following, 9 Followers · all the things code: fediverse/3d printing' name='description'>
<meta content="https://fosstodon.org/@qwazix" property="og:url" />
<meta content="Fosstodon" property="og:site_name" />
<meta content="qwazix (@qwazix@fosstodon.org)" property="og:title" />
<meta content="10 Toots, 9 Following, 9 Followers · all the things code: fediverse/3d printing" property="og:description" />
<meta content="https://cf.mastohost.com/v1/AUTH_91eb37814936490c95da7b85993cc2ff/fosstodon/accounts/avatars/000/036/656/original/JLAPQKYCFLB1.png" property="og:image" />
<meta content="120" property="og:image:width" />
<meta content="120" property="og:image:height" />
<meta content="summary" property="twitter:card" />
<meta content="qwazix@fosstodon.org" property="profile:username" />

<script src="/packs/js/public-b20742e613553ce1df74.chunk.js" crossorigin="anonymous"></script>

</head>
<body class='with-modals theme-default no-reduce-motion'>
<div class='public-layout'>
<div class='container'>
<nav class='header'>
<div class='nav-left'>
<a class="brand" href="https://fosstodon.org/"><svg viewBox="0 0 713.35878 175.8678"><use xlink:href="#mastodon-svg-logo-full" /></svg>
</a><a class="nav-link optional" href="/explore">Profile directory</a>
<a class="nav-link optional" href="/about/more">About</a>
<a class="nav-link optional" href="https://joinmastodon.org/apps">Mobile apps</a>
</div>
<div class='nav-center'></div>
<div class='nav-right'>
<a class="webapp-btn nav-link nav-button" href="/auth/sign_in">Log in</a>
<a class="webapp-btn nav-link nav-button" href="/auth/sign_up">Sign up</a>
</div>
</nav>
</div>
<div class='container'><div class='public-account-header'>
<div class='public-account-header__image'>
<img class="parallax" src="/headers/original/missing.png" />
</div>
<div class='public-account-header__bar'>
<a class="avatar" href="https://fosstodon.org/@qwazix"><img id="profile_page_avatar" data-original="https://cf.mastohost.com/v1/AUTH_91eb37814936490c95da7b85993cc2ff/fosstodon/accounts/avatars/000/036/656/original/JLAPQKYCFLB1.png" data-static="https://cf.mastohost.com/v1/AUTH_91eb37814936490c95da7b85993cc2ff/fosstodon/accounts/avatars/000/036/656/original/JLAPQKYCFLB1.png" src="https://cf.mastohost.com/v1/AUTH_91eb37814936490c95da7b85993cc2ff/fosstodon/accounts/avatars/000/036/656/original/JLAPQKYCFLB1.png" />
</a><div class='public-account-header__tabs'>
<div class='public-account-header__tabs__name'>
<h1>
qwazix
<small>
@qwazix@fosstodon.org

</small>
</h1>
</div>
<div class='public-account-header__tabs__tabs'>
<div class='details-counters'>
<div class='active counter'>
<a class="u-url u-uid" title="10" href="https://fosstodon.org/@qwazix"><span class='counter-number'>10</span>
<span class='counter-label'>Toots</span>
</a></div>
<div class='counter'>
<a title="9" href="https://fosstodon.org/users/qwazix/following"><span class='counter-number'>9</span>
<span class='counter-label'>Following</span>
</a></div>
<div class='counter'>
<a title="9" href="https://fosstodon.org/users/qwazix/followers"><span class='counter-number'>9</span>
<span class='counter-label'>Followers</span>
</a></div>
</div>
<div class='spacer'></div>
<div class='public-account-header__tabs__tabs__buttons'>
<a class="button logo-button modal-button" target="_new" href="/users/qwazix/remote_follow"><svg viewBox="0 0 216.4144 232.00976"><use xlink:href="#mastodon-svg-logo" /></svg>Follow</a>
</div>
</div>
</div>
<div class='public-account-header__extra'>
<div class='public-account-bio'>

<div class='account__header__content emojify'><p>all the things code: fediverse/3d printing</p></div>
<div class='public-account-bio__extra'>
Joined Jul 2018
</div>
</div>

<div class='public-account-header__extra__links'>
<a href="https://fosstodon.org/users/qwazix/following"><strong>9</strong>
Following
</a><a href="https://fosstodon.org/users/qwazix/followers"><strong>9</strong>
Followers
</a></div>
</div>
</div>
</div>

<div class='grid'>
<div class='column-0'>
<div class='h-feed'>
<data class='p-name' value='qwazix on fosstodon.org'>
<div class='account__section-headline'>
<a class="active" href="https://fosstodon.org/@qwazix">Toots</a>
<a class="" href="https://fosstodon.org/@qwazix/with_replies">Toots and replies</a>
<a class="" href="https://fosstodon.org/@qwazix/media">Media</a>
</div>
<div class='activity-stream activity-stream--under-tabs'>

<div class='entry entry-reblog h-cite'>
<div class='status__prepend'>
<div class='status__prepend-icon-wrapper'>
<i class='status__prepend-icon fa fa-fw fa-retweet'></i>
</div>
<span>
<a class="status__display-name muted" href="https://fosstodon.org/@qwazix"><bdi>
<strong class='emojify'>qwazix</strong>
</bdi>
</a>boosted
</span>
</div>
<div class='status'>
```

</div>

</section><section>

## `curl https://mixt.qwazix.com --header "Accept: application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\""`
## `curl https://pleroma.site --header "Accept: application/activity+json; profile=\"https://www.w3.org/ns/activitystreams\""`

### these both return html

(All these are clearly wrong, but this is clear only in hindsight)

<!-- Remember that this is from the perspective of someone who had no idea. The fact that they are wrong is clear now but only in hindsight. -->

</section><section>

## invalid character '<' looking for beginning of value

</section><section>

## missing header 'host'

The "default" headers to be signed with http signatures are *date* *host* *digest* and maybe *content-type*. The go-fed library however did not set the host header leading to the above error.

<!-- While the example httpsig call did have the host set to be signed, the activity package didn't set a host resulting to this error. Another thing that seems obvious now but it did take me a while to figure it out -->

</section><section>

## ok, let's try **@Gargron**'s example

![mastodon blog](mastodonblog.png)

</section><section>

![Internal Server Error](500.png)

</section><section>

## There are many many stars that have to align
![](stars.svg)

1. Https, hostnames, port forwarding and reverse proxies
2. Your server must be able to serve requests while waiting for a response
3. Webfinger must work and id's must be the same
5. Public Keys are saved on the other end
6. Implementations deviate from the spec
7. Cache!

Thus it's very hard to trial-and-error.

<!-- Not getting the expected answer doesn't mean what you sent is wrong. It might be a cached response, the server holding on to an old public key or a request to an endpoint you haven't het built failing -->

</section><section>

## Go-fed

### Go fed is an ActivityPub library in Go.

### It is very formal and very feature-full.
### Not really suited for simple applications.

</section><section>

## Go-fed

### Every vocabulary member of ActivityPub has it's own type in GoFed.
### The interfaces require broad functionality.
### It often disagrees with implementations.

</section><section>

## Whohoo!

![](follow-notification.png)

</section><section>

## how to unfollow?

### The word unfollow doesn't appear in the spec.

1. Implement accepting followers
2. Sniff unfollow from mastodon
3. Try to replicate

(It still doesn't work, I don't know why)

</section><section>

## In other words...
![](blobugh.svg)

</section><section>

## What can we do about it?

</section><section>

## Wiki!

### Mastodon
### Writefreely
### Pleroma
### Pixelfed
### PeerTube
### ...

</section><section>

## Wiki!

## Writefreely
### New Post

``` json

{
  "@context": [
    "https://www.w3.org/ns/activitystreams",
    {
      "commentsEnabled": "sc:Boolean",
      "sc": "http://schema.org#"
    }
  ],
  "type": "Create",
  "id": "http://writefreely.xps/api/posts/3y2vettm5r",
  "actor": "http://writefreely.xps/api/collections/qwazix",
  "published": "2019-08-08T16:02:49.613334687Z",
  "to": [
    "https://www.w3.org/ns/activitystreams#Public"
  ],
  "cc": [
    "http://floorb.qwazix.com/actor/"
  ],
  "object": {
    "type": "Article",
    "id": "http://writefreely.xps/api/posts/3y2vettm5r",
    "published": "2019-08-08T16:02:49.613334687Z",
    "inReplyTo": null,
    "url": "http://writefreely.xps/this-is-post-number-3",
    "attributedTo": "http://writefreely.xps/api/collections/qwazix",
    "to": [
      "https://www.w3.org/ns/activitystreams#Public"
    ],
    "cc": [
      "http://floorb.qwazix.com/actor/"
    ],
    "name": "This is post number 3",
    "content": "\\u003cp\\u003epost 3\\u003c/p\\u003e\n",
    "contentMap": {
      "en": "\\u003cp\\u003epost 3\\u003c/p\\u003e\n"
    },
    "tag": [],
    "commentsEnabled": false
  }
}

```
...
</section><section>

## Pherephone
### Follow

``` json

{
  "@context": "https://www.w3.org/ns/activitystreams",
  "actor": "http://floorb.qwazix.com/nuerList",
  "object": "http://writefreely.xps/api/collections/qwazix",
  "to": "http://writefreely.xps/api/collections/qwazix",
  "type": "Follow"
}

```
</section><section>

## Pherephone
### And don't forget the headers

``` yaml

Rec'd "POST /api/collections/qwazix/inbox HTTP/1.1
Host: writefreely.xps
Accept-Charset: utf-8
Accept-Encoding: gzip
Connection: Keep-Alive
Content-Length: 222
Content-Type: application/ld+json; profile="https://www.w3.org/ns/activitystreams"
Date: Tue, 13 Aug 2019 11:18:35 GMT
Digest: SHA-256=nZV7ldUDtA5yCqFGbjA+5t0hQeDCce5vFDJYL8aAVOo=
Signature: keyId="",algorithm="rsa-sha256",headers="(request-target) date host
User-Agent: pherephone (go-fed/activity v1.0.0)
X-Forwarded-For: 127.0.0.1
X-Forwarded-Host: writefreely.xps
X-Forwarded-Server: writefreely.xps

```
</section><section>

## Mandate wiki in the spec?

<!-- ### Is it going too far to say that we could require adding your app to the wiki if you want to be able to clame to be compliant? -->

</section><section>

## Developer mode

### Toggle security features (TLS, httpsignatures)
### No cache
### Easy purging of account information

</section><section>

## But what is pherephone?

<center> _pherephonon_ is a Greek word that means _someone who doesn't have his own voice and just repeats what others say_ </center>

</section><section>

## Pherephone is an ActivityPub repeater

You tell it to follow some accounts and it "Announces" all their posts.

</section><section>

## Uses

1. Curated lists
2. Follow instance timeline ("boost" everything in an instance)
3. Follow all the different accounts of the same person (Blog, Photos, Videos)
4. What will you make of it!

</section><section>

## Current state

1. Written in **go**
2. Standalone binary for Linux
3. JSON input of a list of actors to follow
``` json
{
    "writefreelyAndFriends" : {
        "summary": "a list of favorite writers",
        "follow": [
            "https://write.as/api/collections/blog",
            "https://writing.exchange/users/write_as"
        ]
    }
}
```
4. No webfinger lookup for following accounts
5. Doesn't work with Pleroma yet

</section><section>

## Immediate Goals

1. Usable as a library
2. Paging (right now pherephone spits everything in a single `OrderedCollectionPage`)
3. Fix unfollowing

</section><section>

## Non-immediate Goals

### @pherephone@qwazix.com follow @matt@writing.exchange

1. Administration via activityPub (set an admin account url and public key who will be able to follow/unfollow account or change settings just by mentioning and issuing commands)
2. Avatars
3. `/nodeInfo` and `/statistics` endpoints

I would like to see this in other servers too

</section><section>

## tl;dl

### we need to create a better onboarding experience

### pherephone is an activityPub repeater

</section>